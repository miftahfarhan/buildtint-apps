import React, { useEffect, useState } from "react";
import {
  Image,
  ImageBackground,
  SafeAreaView,
  ScrollView,
  Text,
  View,
} from "react-native";
import * as SecureStore from "expo-secure-store";
import { fetchInvitationList } from "../network/lib/invitation";
import {
  VStack,
  Surface,
  HStack,
  Flex,
  ActivityIndicator,
  Pressable,
} from "@react-native-material/core";
import Icon from "react-native-vector-icons/AntDesign";

import moment from "moment";
import { fetchLogout } from "../network/lib/auth";
import { API_URL } from "../utils/urls";

function HomeScreen({ navigation }) {
  async function checkIsLogin() {
    let result = await SecureStore.getItemAsync("token");
    if (!result) {
      alert("Silakan login terlebih dahulu");
      navigation.navigate("Login");
    }
  }

  const [isloading, setIsloading] = useState(false);
  const [token, settoken] = useState("");
  const [products, setProducts] = useState([]);
  const [name, setname] = useState("");

  async function getValueFor(key) {
    let result = await SecureStore.getItemAsync(key);
    setname(result);
  }

  const getInvitation = async () => {
    setIsloading(true);
    let token = await SecureStore.getItemAsync("token");
    settoken(token);
    await fetchInvitationList(token)
      .then((response) => {
        setIsloading(false);
        setProducts(response.data.result);
      })
      .catch((error) => {
        setIsloading(false);
        navigation.navigate("Login");
      });
  };

  useEffect(() => {
    // checkIsLogin();
    getValueFor("name");
    getInvitation();
  }, []);

  const logout = async () => {
    await fetchLogout(token)
      .then((response) => {
        // localStorage.clear();
        SecureStore.deleteItemAsync("token");
        navigation.navigate("Login");
      })
      .catch((error) => {
        // localStorage.clear();
        SecureStore.deleteItemAsync("token");
        navigation.navigate("Login");
      });
  };

  return (
    <SafeAreaView>
      {/* <AppBar title="Buildtint" color="#019FC4" tintColor="white" /> */}
      <View
        style={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          padding: 16,
          paddingTop: 32,
          backgroundColor: "white",
          justifyContent: "space-between",
        }}
      >
        <Image
          style={{
            width: 100,
            height: 24,
            resizeMode: "contain",
          }}
          source={require("../assets/buildtint-logo-text.png")}
        />
        <Pressable style={{ padding: 8 }} onPress={logout}>
          <Icon name="logout" color="red" />
        </Pressable>
      </View>

      <View
        style={{
          padding: 16,
          display: "flex",
          flexDirection: "column",
        }}
      >
        <Text style={{ marginBottom: 4, fontSize: 18, fontWeight: "700" }}>
          Selamat Datang, {name}!
        </Text>
        <Text style={{ marginBottom: 16, fontSize: 14, fontWeight: "400" }}>
          Silakan pilih undangan anda untuk memulai
        </Text>
        {isloading ? (
          <>
            <ActivityIndicator size="large" color="#019FC4" />
          </>
        ) : (
          <>
            <ScrollView>
              <VStack spacing={16} style={{ marginBottom: 120 }}>
                {products
                  .filter((fil) => fil.status !== "drafted")
                  .map((prod) => (
                    <Pressable
                      onPress={() =>
                        navigation.navigate("App", {
                          _id: prod?._id,
                          bride_name:
                            prod?.invitation_data?.bride_data?.nick_name,
                          groom_name:
                            prod?.invitation_data?.groom_data?.nick_name,
                          main_image: prod?.invitation_data?.main_image,
                        })
                      }
                      style={{ borderRadius: 8 }}
                      key={prod?._id}
                    >
                      <Surface
                        elevation={2}
                        category="medium"
                        // style={{ borderRadius: "16px" }}
                        style={{ padding: 0, borderRadius: 8 }}
                      >
                        <ImageBackground
                          source={{
                            uri: `${API_URL}${prod?.invitation_data?.main_image}`,
                          }}
                          imageStyle={{ borderRadius: 8 }}
                        >
                          <HStack
                            spacing={8}
                            alignItems="center"
                            style={{
                              backgroundColor: "rgba(0, 0, 0, 0.70)",
                              borderRadius: 8,
                            }}
                          >
                            <Image
                              style={{
                                width: 90,
                                height: 130,
                                resizeMode: "cover",
                                borderRadius: 8,
                              }}
                              source={{
                                uri: `${API_URL}${prod?.invitation_data?.main_image}`,
                              }}
                            />
                            <Flex direction="column">
                              <VStack spacing={2} style={{ marginBottom: 8 }}>
                                <HStack
                                  spacing={8}
                                  style={{ alignItems: "center" }}
                                >
                                  <Text
                                    style={{
                                      fontWeight: "700",
                                      fontSize: 16,
                                      color: "white",
                                    }}
                                  >
                                    {prod?.invitation_data?.bride_data
                                      ?.nick_name &&
                                    prod?.invitation_data?.groom_data?.nick_name
                                      ? `${prod?.invitation_data?.bride_data?.nick_name} & ${prod?.invitation_data?.groom_data?.nick_name}`
                                      : "Untitled"}
                                  </Text>
                                  <Icon name="checkcircle" color="green" />
                                </HStack>
                                <Text
                                  style={{
                                    fontWeight: "400",
                                    fontSize: 12,
                                    color: "grey",
                                  }}
                                >
                                  Updated{" "}
                                  {moment(new Date(prod?.updated_at)).format(
                                    "DD MMMM YYYY, hh:mm"
                                  )}
                                </Text>
                              </VStack>
                              {/* <Pressable
                              onPress={() =>
                                navigation.navigate("Scanner", {
                                  _id: prod?._id,
                                  bride_name:
                                    prod?.invitation_data?.bride_data
                                      ?.nick_name,
                                  groom_name:
                                    prod?.invitation_data?.groom_data
                                      ?.nick_name,
                                })
                              }
                            >
                              <HStack alignItems="center" spacing={6}>
                                <IconIon
                                  name="book-outline"
                                  color="#019FC4"
                                  style={{ fontSize: 16 }}
                                />
                                <Text
                                  style={{ fontWeight: "500", fontSize: 16 }}
                                >
                                  Buku Tamu
                                </Text>
                              </HStack>
                            </Pressable> */}
                            </Flex>
                          </HStack>
                        </ImageBackground>
                      </Surface>
                    </Pressable>
                  ))}
              </VStack>
            </ScrollView>
          </>
        )}
      </View>
    </SafeAreaView>
  );
}

export default HomeScreen;
