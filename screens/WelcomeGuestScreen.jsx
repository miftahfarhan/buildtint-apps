import { Box, Flex } from "@react-native-material/core";
import React, { useEffect, useState } from "react";
import { Image, SafeAreaView, StyleSheet, Text } from "react-native";
import LottieView from "lottie-react-native";

function WelcomeGuestScreen({ navigation, route }) {
  const _id = route.params._id;
  const [seccond, setseccond] = useState(5);
  const guest_name = route.params.guest_name;
  const bride_name = route.params.bride_name;
  const groom_name = route.params.groom_name;
  const main_image = route.params.main_image;

  useEffect(() => {
    setTimeout(() => {
      setseccond(4);
    }, 1000);
    setTimeout(() => {
      setseccond(3);
    }, 2000);
    setTimeout(() => {
      setseccond(2);
    }, 3000);
    setTimeout(() => {
      setseccond(1);
    }, 4000);
    setTimeout(() => {
      navigation.navigate("App", {
        _id: _id,
        bride_name: bride_name,
        groom_name: groom_name,
        main_image: main_image,
      });
      setseccond(5);
    }, 5000);
  }, []);

  return (
    <>
      <SafeAreaView style={styles.container}>
        <Flex direction="column" style={{ alignItems: "center" }}>
          <Image
            style={{
              //   width: 100,
              height: 30,
              resizeMode: "contain",
              marginBottom: 32,
            }}
            source={require("../assets/buildtint-logo-text.png")}
          />
          <Text
            style={{
              fontSize: 32,
              color: "white",
              fontWeight: "700",
              marginBottom: 32,
            }}
          >
            Selamat Datang
          </Text>

          <LottieView
            autoPlay
            style={{
              width: 150,
              height: 150,
            }}
            // Find more Lottie files at https://lottiefiles.com/featured
            source={require("../assets/lottie/97240-success.json")}
          />

          <Text
            style={{
              textAlign: "center",
              fontSize: 60,
              color: "white",
              fontWeight: "700",
              marginBottom: 52,
              marginTop: 16,
            }}
          >
            {guest_name}
          </Text>
          <Text
            style={{
              textAlign: "center",
              fontSize: 16,
              color: "white",
              fontWeight: "400",
              marginBottom: 16,
            }}
          >
            Selamat datang di Pernikahan
          </Text>
          <Text
            style={{
              fontSize: 32,
              color: "white",
              fontWeight: "700",
              marginBottom: 4,
            }}
          >
            {bride_name}
          </Text>
          <Text
            style={{
              fontSize: 16,
              color: "white",
              fontWeight: "700",
              marginBottom: 4,
            }}
          >
            &
          </Text>
          <Text
            style={{
              fontSize: 32,
              color: "white",
              fontWeight: "700",
              marginBottom: 32,
            }}
          >
            {groom_name}
          </Text>

          <Text
            style={{
              textAlign: "center",
              fontSize: 16,
              color: "white",
              fontWeight: "400",
              marginBottom: 16,
            }}
          >
            Scanner akan kembali aktif dalam
          </Text>

          <Box
            style={{
              borderColor: "white",
              borderWidth: 1,
              width: 50,
              height: 50,
              borderRadius: 100,
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Text style={{ fontSize: 24, color: "white", fontWeight: "700" }}>
              {seccond}
            </Text>
          </Box>
        </Flex>
      </SafeAreaView>
    </>
  );
}

export default WelcomeGuestScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
    backgroundColor: "#25ABE1",
    alignItems: "center",
    justifyContent: "center",
  },
});
