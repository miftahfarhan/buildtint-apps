import React, { useEffect, useMemo, useState } from "react";
import {
  FlatList,
  Image,
  ImageBackground,
  SafeAreaView,
  ScrollView,
  Text,
  View,
} from "react-native";
import * as SecureStore from "expo-secure-store";
import {
  fetchAddScanGuest,
  fetchInvitationGuest,
  fetchScan,
} from "../network/lib/invitation";
import {
  VStack,
  Flex,
  Pressable,
  Spacer,
  HStack,
} from "@react-native-material/core";
import Icon from "react-native-vector-icons/AntDesign";

import { API_URL } from "../utils/urls";
import {
  ActivityIndicator,
  Avatar,
  Button,
  Dialog,
  Portal,
  Provider,
  Snackbar,
} from "react-native-paper";
import { TextInput } from "@react-native-material/core";

import { BottomSheet } from "react-native-btr";
import { GuestItem } from "../components/GuestItem";
import { useNavigation } from "@react-navigation/native";

function AppScreen({ navigation, route }) {
  const navigationProps = useNavigation();

  const [responseMessage, setResponseMessage] = useState("");
  const [showSnackbar, setShowSnackbar] = useState(false);
  const onDismissSnackBar = () => setShowSnackbar(false);

  const invitationId = route.params._id;
  const bride_name = route.params.bride_name;
  const groom_name = route.params.groom_name;
  const main_image = route.params.main_image;

  const [visible, setVisible] = useState(false);

  const toggleBottomNavigationView = () => {
    setVisible(!visible);
  };

  const [isloading, setIsloading] = useState(false);
  const [searchKeyword, setsearchKeyword] = useState("");
  const [token, settoken] = useState("");
  const [guests, setGuests] = useState([]);
  const [page, setpage] = useState(1);

  const getInvitation = async (page) => {
    setIsloading(true);
    let token = await SecureStore.getItemAsync("token");
    settoken(token);
    await fetchInvitationGuest(token, invitationId, page, searchKeyword)
      .then((response) => {
        setIsloading(false);
        setGuests(response.data.result.data);
      })
      .catch((error) => {
        setIsloading(false);
      });
  };

  const getInvitationLoadMore = async () => {
    if (page === 1) return;

    let token = await SecureStore.getItemAsync("token");
    settoken(token);
    await fetchInvitationGuest(token, invitationId, page, searchKeyword)
      .then((response) => {
        const lastPage = response?.data?.result?.last_page;
        if (page === lastPage) return;
        var newGuest = [...guests];
        response?.data?.result?.data?.map((data) => {
          newGuest.push(data);
        });
        setGuests(newGuest);
      })
      .catch((error) => {});
  };

  useEffect(() => {
    getInvitationLoadMore();
  }, [page]);

  const search = (value) => {
    getInvitation("1");
    setpage(1);
  };

  useEffect(() => {
    // checkIsLogin();
    getInvitation("1");
  }, []);

  const [nama, setNama] = useState("");
  const [keterangan, setKeterangan] = useState("");
  const [isLoadingAddGuest, setisLoadingAddGuest] = useState(false);

  const onAddGuest = async () => {
    if (!nama) {
      alert("Harap isi nama tamu");
      return;
    }

    setisLoadingAddGuest(true);
    let token = await SecureStore.getItemAsync("token");
    settoken(token);

    await fetchAddScanGuest(token, invitationId, {
      guest_name: nama,
      guest_type: "individu",
      guest_label: keterangan,
      channel: "1",
    })
      .then((response) => {
        toggleBottomNavigationView();

        setisLoadingAddGuest(false);
        navigation.navigate("WelcomeGuest", {
          _id: invitationId,
          guest_name: nama,
          bride_name: bride_name,
          groom_name: groom_name,
          main_image: main_image,
        });
        getInvitation("1");
        setpage(1);
        setNama("");
      })
      .catch((error) => {
        toggleBottomNavigationView();

        setisLoadingAddGuest(false);
        alert(`Gagal menambah tamu, ${error?.response?.data?.message}`);
      });
  };

  return (
    <Provider>
      <SafeAreaView>
        <View
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            padding: 16,
            paddingTop: 32,
            backgroundColor: "white",
            justifyContent: "space-between",
          }}
        >
          <Image
            style={{
              width: 100,
              height: 24,
              resizeMode: "contain",
            }}
            source={require("../assets/buildtint-logo-text.png")}
          />
        </View>

        <View
          style={{
            display: "flex",
            flexDirection: "column",
          }}
        >
          <View style={{ position: "relative", marginBottom: 48 }}>
            <ImageBackground
              source={{
                uri: `${API_URL}${main_image}`,
              }}
              imageStyle={{
                borderBottomLeftRadius: 50,
                borderBottomRightRadius: 50,
              }}
            >
              <View
                style={{
                  paddingHorizontal: 16,
                  paddingTop: 100,
                  paddingBottom: 120,
                  backgroundColor: main_image
                    ? "rgba(0, 0, 0, 0.70)"
                    : "#019FC4",
                  borderBottomLeftRadius: 50,
                  borderBottomRightRadius: 50,
                }}
              >
                <Text
                  style={{
                    fontWeight: "700",
                    fontSize: 24,
                    color: "white",
                  }}
                >
                  {bride_name && groom_name
                    ? `Hi, ${bride_name} & ${groom_name}`
                    : "Untitled"}
                </Text>
                <Text
                  style={{
                    fontWeight: "300",
                    fontSize: 12,
                    color: "white",
                  }}
                >
                  Klik mulai scan untuk memulai scan QR code
                </Text>
              </View>
            </ImageBackground>
            <View
              style={{
                //   backgroundColor: "blue",
                position: "absolute",
                zIndex: 99,
                bottom: -30,
                left: 0,
                right: 0,
              }}
            >
              <Flex direction="column" alignItems="center">
                <Pressable
                  onPress={() =>
                    navigation.navigate("Scanner", {
                      _id: invitationId,
                      bride_name: bride_name,
                      groom_name: groom_name,
                    })
                  }
                  style={{ borderRadius: 99 }}
                >
                  <View
                    style={{
                      padding: 16,
                      justifyContent: "center",
                      backgroundColor: "white",
                      elevation: 4,
                      borderRadius: 99,
                    }}
                  >
                    <Flex direction="row" alignItems="center">
                      <Icon
                        name="qrcode"
                        color="#019FC4"
                        style={{ fontSize: 30 }}
                      />

                      <Text
                        style={{
                          textAlign: "center",
                          marginLeft: 8,
                          fontWeight: "700",
                        }}
                      >
                        Mulai Scan QR Code
                      </Text>
                    </Flex>
                  </View>
                </Pressable>
              </Flex>
            </View>
          </View>

          <View style={{ padding: 16 }}>
            <Flex direction="column">
              <Text style={{ fontWeight: "700", fontSize: 18 }}>
                Daftar Tamu
              </Text>
              <Text
                style={{
                  fontWeight: "400",
                  marginBottom: 16,
                  color: "grey",
                  fontSize: 12,
                }}
              >
                Cari nama tamu, klik tombol Checkin apabila tamu hadir
              </Text>

              <Button
                icon="plus"
                mode="contained"
                style={{ borderRadius: 8, marginBottom: 16, color: "white" }}
                buttonColor="#019FC4"
                onPress={toggleBottomNavigationView}
                textColor="white"
              >
                Tambah Tamu Baru
              </Button>

              <View>
                <HStack spacing={8} alignItems="center">
                  <TextInput
                    variant="outlined"
                    placeholder="Cari nama tamu"
                    style={{ width: 620, marginBottom: 16, marginRight: 8 }}
                    color="#019FC4"
                    autoCapitalize="none"
                    onChangeText={setsearchKeyword}
                    value={searchKeyword}
                  />
                  <Button
                    icon="magnify"
                    mode="contained"
                    style={{
                      borderRadius: 8,
                      marginBottom: 16,
                      color: "white",
                    }}
                    buttonColor="#019FC4"
                    onPress={search}
                    textColor="white"
                  >
                    Cari Tamu
                  </Button>
                </HStack>
              </View>

              {isloading ? (
                <>
                  <View style={{ zIndex: 99, height: "100%" }}>
                    <Flex direction="column" alignItems="center">
                      <ActivityIndicator size="large" color="#019FC4" />
                      <Text
                        style={{
                          color: "black",
                          fontWeight: "700",
                          marginTop: 8,
                        }}
                      >
                        Loading...
                      </Text>
                    </Flex>
                  </View>
                </>
              ) : (
                <>
                  <View style={{ maxHeight: 600 }}>
                    <FlatList
                      data={guests}
                      extraData={guests}
                      renderItem={({ item }) => (
                        <GuestItem
                          _id={item?._id}
                          guest_name={item?.guest_name}
                          guest_label={item?.guest_label}
                          guest_type={item?.guest_type}
                          presence={item?.presence}
                          invitationId={invitationId}
                          navigation={navigationProps}
                          bride_name={bride_name}
                          groom_name={groom_name}
                          main_image={main_image}
                          onSuccess={() => {
                            getInvitation("1");
                            setpage(1);
                          }}
                        />
                      )}
                      keyExtractor={(item) => item?._id}
                      onEndReached={() => setpage((page) => (page += 1))}
                    />
                  </View>
                </>
              )}
            </Flex>
          </View>

          <BottomSheet
            visible={visible}
            onBackButtonPress={toggleBottomNavigationView}
            onBackdropPress={toggleBottomNavigationView}
          >
            <View
              style={{
                backgroundColor: "white",
                padding: 16,
              }}
            >
              <Flex direction="column">
                <Text
                  style={{
                    fontWeight: "700",
                    marginBottom: 24,
                    fontSize: 18,
                  }}
                >
                  Tambah Tamu Baru
                </Text>

                <Text>Nama Tamu</Text>
                <TextInput
                  variant="outlined"
                  placeholder="Nama Tamu"
                  style={{ width: "100%", marginBottom: 16 }}
                  color="#019FC4"
                  autoCapitalize="none"
                  onChangeText={setNama}
                  value={nama}
                />

                <Text>Keterangan Tamu</Text>
                <TextInput
                  variant="outlined"
                  placeholder="Keterangan Tamu"
                  style={{ width: "100%", marginBottom: 16 }}
                  color="#019FC4"
                  autoCapitalize="none"
                  onChangeText={setKeterangan}
                  value={keterangan}
                />

                <Button
                  mode="contained"
                  style={{ borderRadius: 8 }}
                  buttonColor="#019FC4"
                  icon="plus"
                  loading={isLoadingAddGuest}
                  onPress={() => onAddGuest()}
                >
                  Tambah Tamu
                </Button>
              </Flex>
            </View>
          </BottomSheet>

          <Snackbar
            visible={showSnackbar}
            onDismiss={onDismissSnackBar}
            duration={2000}
          >
            {responseMessage}
          </Snackbar>
        </View>
      </SafeAreaView>
    </Provider>
  );
}

export default AppScreen;
