import { Image, SafeAreaView, StyleSheet, Text, View } from "react-native";
import React, { useEffect, useState } from "react";
import { fetchLogin } from "../network/lib/auth";
import { Button, TextInput, VStack } from "@react-native-material/core";
import { Snackbar } from "react-native-paper";
import Icon from "react-native-vector-icons/AntDesign";
import * as SecureStore from "expo-secure-store";

async function save(key, value) {
  await SecureStore.setItemAsync(key, value);
}

const LoginScreen = ({ navigation }) => {
  async function getValueFor(key) {
    let result = await SecureStore.getItemAsync(key);
    if (result) {
      navigation.navigate("Home");
    }
  }

  useEffect(() => {
    getValueFor("token");
  }, []);

  const [responseMessage, setResponseMessage] = useState("");
  const [showSnackbar, setShowSnackbar] = useState(false);
  const onDismissSnackBar = () => setShowSnackbar(false);
  const [isloading, setIsloading] = useState(false);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const login = async () => {
    setIsloading(true);
    if (!username) {
      setIsloading(false);
      setResponseMessage("Masukkan email anda");
      setShowSnackbar(true);
      return;
    }

    if (!password) {
      setIsloading(false);
      setResponseMessage("Masukkan password anda");
      setShowSnackbar(true);
      return;
    }

    await fetchLogin({
      email: username,
      password: password,
      authType: "basic_auth",
    })
      .then((response) => {
        save("token", response?.data?.token);
        save("name", response?.data?.user?.name);
        save("email", response?.data?.user?.email);
        setIsloading(false);
        navigation.navigate("Home");
      })
      .catch((error) => {
        setIsloading(false);
        setResponseMessage(error?.response?.data?.message);
        setShowSnackbar(true);
        console.log(error);
      });
  };
  return (
    <SafeAreaView style={styles.container}>
      <View
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          width: "100%",
          padding: 16,
          maxWidth: 400,
        }}
      >
        <Image
          style={{
            //   width: 100,
            height: 30,
            resizeMode: "contain",
            marginBottom: 16,
          }}
          source={require("../assets/buildtint-logo-text.png")}
        />
        <Text style={{ fontSize: 18, fontWeight: "700", marginBottom: 16 }}>
          Selamat Datang!
        </Text>

        <VStack style={{ width: "100%" }} spacing={4}>
          <Text style={{ fontSize: 14 }}>Email</Text>
          <TextInput
            variant="outlined"
            placeholder="Email"
            style={{ width: "100%", marginBottom: 16 }}
            color="#019FC4"
            autoCapitalize="none"
            onChangeText={setUsername}
          />
        </VStack>
        <VStack style={{ width: "100%" }} spacing={4}>
          <Text style={{ fontSize: 14 }}>Password</Text>
          <TextInput
            variant="outlined"
            placeholder="Password"
            style={{ width: "100%", marginBottom: 16 }}
            color="#019FC4"
            onChangeText={setPassword}
            autoCapitalize="none"
            secureTextEntry
          />
        </VStack>

        <Button
          color="#019FC4"
          tintColor="white"
          title="Login"
          style={{ width: "100%" }}
          loading={isloading}
          onPress={() => login()}
        />
      </View>
      <Snackbar
        visible={showSnackbar}
        onDismiss={onDismissSnackBar}
        duration={2000}
      >
        {responseMessage}
      </Snackbar>
    </SafeAreaView>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: 8,
    paddingHorizontal: 32,
    borderRadius: 4,
    elevation: 3,
    width: "100%",
    backgroundColor: "#25ABE1",
  },
  text: {
    fontSize: 16,
    lineHeight: 21,
    fontWeight: "bold",
    letterSpacing: 0.25,
    color: "white",
  },
  input: {
    height: 50,
    width: "100%",
    marginBottom: 16,
    borderWidth: 1,
    borderColor: "grey",
    padding: 10,
    borderRadius: 4,
  },
});
