import React, { useState, useEffect } from "react";
import {
  Text,
  View,
  StyleSheet,
  Button,
  SafeAreaView,
  Image,
} from "react-native";
import { BarCodeScanner } from "expo-barcode-scanner";
import Icon from "react-native-vector-icons/FontAwesome";
import IconMaterial from "react-native-vector-icons/MaterialIcons";
import { fetchScan } from "../network/lib/invitation";
import * as SecureStore from "expo-secure-store";
import Spinner from "react-native-loading-spinner-overlay";
import { Camera } from "expo-camera";
import { useIsFocused } from "@react-navigation/native";
import LottieView from "lottie-react-native";
import { Box, Flex } from "@react-native-material/core";
import {
  useFonts,
  GreatVibes_400Regular,
} from "@expo-google-fonts/great-vibes";

export default function ScannerScreen({ navigation, route }) {
  let [fontsLoaded] = useFonts({
    GreatVibes_400Regular,
  });
  const invitationId = route.params._id;
  const bride_name = route.params.bride_name;
  const groom_name = route.params.groom_name;

  const [token, settoken] = useState("");
  const [type, setType] = useState("front");
  const [hasPermission, setHasPermission] = useState(null);
  const [scanned, setScanned] = useState(false);
  const isFocused = useIsFocused();

  const [guest_name, setguestName] = useState("");
  const [seccond, setseccond] = useState(5);
  const [isSuccess, setisSuccess] = useState(false);

  async function checkIsLogin() {
    let result = await SecureStore.getItemAsync("token");
    settoken(result);
    if (!result) {
      alert("Silakan login terlebih dahulu");
      navigation.navigate("Login");
    }
  }

  useEffect(() => {
    setScanned(false);
    checkIsLogin();
  }, []);

  const scanQR = async (guest_id) => {
    await fetchScan(token, invitationId, {
      guest_id: guest_id,
      channel: 1,
    })
      .then((response) => {
        if (response.data.message !== "Tamu sudah hadir") {
          setisSuccess(true);
          setguestName(response.data?.result?.guest_name);
          // navigation.navigate("WelcomeGuest", {
          //   _id: invitationId,
          //   guest_name: response.data?.result?.guest_name,
          //   bride_name: bride_name,
          //   groom_name: groom_name,
          // });
          setTimeout(() => {
            setseccond(4);
          }, 1000);
          setTimeout(() => {
            setseccond(3);
          }, 2000);
          setTimeout(() => {
            setseccond(2);
          }, 3000);
          setTimeout(() => {
            setseccond(1);
          }, 4000);
          setTimeout(() => {
            setisSuccess(false);
            setScanned(false);
            setseccond(5);
          }, 5000);
        } else {
          setTimeout(() => {
            setScanned(false);
            alert(response.data.message);
          }, 1000);
        }
      })
      .catch((error) => {
        if (error) {
          setTimeout(() => {
            setScanned(false);
            alert(error?.response?.data?.message);
          }, 1000);
        }
      });
  };

  useEffect(() => {
    if (isFocused) {
      const getBarCodeScannerPermissions = async () => {
        const { status } = await BarCodeScanner.requestPermissionsAsync();
        setHasPermission(status === "granted");
      };

      getBarCodeScannerPermissions();
    }
  }, [isFocused]);

  const handleBarCodeScanned = ({ bounds, data }) => {
    setScanned(true);
    scanQR(data);
    // alert(`Bar code with type ${type} and data ${data} has been scanned!`);
  };

  if (hasPermission === null) {
    return <Text>Requesting for camera permission</Text>;
  }
  if (hasPermission === false) {
    return <Text>No access to camera</Text>;
  }

  return (
    // <SafeAreaView style={{ backgroundColor: "#25ABE1" }}>

    //   <Camera
    //     onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
    //     style={{
    //       width: "100%",
    //       height: "100%",
    //       position: "relative",
    //     }}
    //     type={type}
    //     barCodeScannerSettings={{
    //       barCodeTypes: [BarCodeScanner.Constants.BarCodeType.qr],
    //     }}
    //   >
    //     <View
    //       style={{
    //         position: "absolute",
    //         width: "100%",
    //         height: "100%",
    //       }}
    //     >
    //       <LottieView
    //         autoPlay
    //         style={{
    //           width: "100%",
    //           height: "100%",
    //         }}
    //         source={require("../assets/lottie/41671-scan.json")}
    //       />
    //     </View>
    //   </Camera>
    //   <BarCodeScanner
    //     onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
    //     style={{
    //       width: "100%",
    //       height: "100%",
    //       position: "relative",
    //     }}
    //     type={type}
    //   >
    //     <View
    //       style={{
    //         position: "absolute",
    //         top: Y,
    //         left: X,
    //         width: width,
    //         height: height,
    //         borderColor: "red",
    //         borderWidth: 2,
    //       }}
    //     ></View>
    //   </BarCodeScanner>

    //   <Spinner
    //     visible={scanned}
    //     textContent={"Loading..."}
    //     textStyle={{ color: "white" }}
    //   />

    // </SafeAreaView>
    <>
      {!fontsLoaded ? (
        <></>
      ) : (
        <>
          {!isSuccess ? (
            <>
              <View style={StyleSheet.absoluteFill}>
                <View
                  style={{
                    padding: 8,
                    paddingTop: 32,
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                    backgroundColor: "#25ABE1",
                    width: "100%",
                    zIndex: 99,
                  }}
                >
                  <Icon
                    name="chevron-left"
                    size={16}
                    color="white"
                    style={{ marginRight: 16, padding: 8 }}
                    onPress={() => navigation.navigate("Home")}
                  />
                  <Text
                    style={{ color: "white", fontWeight: "700", fontSize: 18 }}
                  >
                    Buildtint Scanner
                  </Text>
                  <IconMaterial
                    name="flip-camera-ios"
                    size={24}
                    color="white"
                    style={{ marginRight: 16, padding: 8 }}
                    onPress={() =>
                      type === "front" ? setType("back") : setType("front")
                    }
                  />
                </View>
                <View
                  style={{
                    position: "absolute",
                    top: 120,
                    zIndex: 99,
                    width: "100%",
                  }}
                >
                  <Text
                    style={{
                      fontSize: 16,
                      textAlign: "center",
                      color: "white",
                      fontWeight: "400",
                    }}
                  >
                    Selamat Datang di Pernikahan
                  </Text>
                  <Text
                    style={{
                      fontSize: 38,
                      color: "white",
                      fontWeight: "400",
                      textAlign: "center",
                      width: "100%",
                      fontFamily: "GreatVibes_400Regular",
                    }}
                  >
                    {bride_name} & {groom_name}
                  </Text>
                </View>

                <BarCodeScanner
                  onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
                  style={[StyleSheet.absoluteFill, styles.container]}
                  type={type}
                >
                  <View style={styles.layerTop} />
                  <View style={styles.layerCenter}>
                    <View style={styles.layerLeft} />
                    <View style={styles.focused} />
                    <View style={styles.layerRight} />
                  </View>
                  <View style={styles.layerBottom} />
                </BarCodeScanner>
                {/* {scanned && (
              <Button
                title={"Tap to Scan Again"}
                onPress={() => setScanned(false)}
              />
            )} */}
                <Spinner
                  visible={scanned}
                  textContent={"Loading..."}
                  textStyle={{ color: "white" }}
                />
              </View>
            </>
          ) : (
            <>
              <SafeAreaView style={styles.container2}>
                <Flex direction="column" style={{ alignItems: "center" }}>
                  <Image
                    style={{
                      //   width: 100,
                      height: 30,
                      resizeMode: "contain",
                      marginBottom: 32,
                    }}
                    source={require("../assets/buildtint-logo-text.png")}
                  />
                  <Text
                    style={{
                      fontSize: 32,
                      color: "white",
                      fontWeight: "700",
                      marginBottom: 32,
                    }}
                  >
                    Selamat Datang
                  </Text>

                  <LottieView
                    autoPlay
                    style={{
                      width: 150,
                      height: 150,
                    }}
                    source={require("../assets/lottie/97240-success.json")}
                  />

                  <Text
                    style={{
                      textAlign: "center",
                      fontSize: 60,
                      color: "white",
                      fontWeight: "700",
                      marginBottom: 52,
                      marginTop: 16,
                    }}
                  >
                    {guest_name}
                  </Text>

                  <Text
                    style={{
                      textAlign: "center",
                      fontSize: 16,
                      color: "white",
                      fontWeight: "400",
                      marginBottom: 16,
                      marginTop: 38,
                    }}
                  >
                    Selamat datang di Pernikahan
                  </Text>
                  <Text
                    style={{
                      fontSize: 32,
                      color: "white",
                      fontWeight: "700",
                      marginBottom: 4,
                    }}
                  >
                    {bride_name}
                  </Text>
                  <Text
                    style={{
                      fontSize: 16,
                      color: "white",
                      fontWeight: "700",
                      marginBottom: 4,
                    }}
                  >
                    &
                  </Text>
                  <Text
                    style={{
                      fontSize: 32,
                      color: "white",
                      fontWeight: "700",
                      marginBottom: 32,
                    }}
                  >
                    {groom_name}
                  </Text>

                  <Text
                    style={{
                      textAlign: "center",
                      fontSize: 16,
                      color: "white",
                      fontWeight: "400",
                      marginBottom: 16,
                    }}
                  >
                    Scanner akan kembali aktif dalam
                  </Text>

                  <Box
                    style={{
                      borderColor: "white",
                      borderWidth: 1,
                      width: 50,
                      height: 50,
                      borderRadius: 100,
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 24,
                        color: "white",
                        fontWeight: "700",
                      }}
                    >
                      {seccond}
                    </Text>
                  </Box>
                </Flex>
              </SafeAreaView>
            </>
          )}
        </>
      )}
    </>
  );
}
const opacity = "rgba(0, 0, 0, .6)";
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#25ABE1",
  },
  container2: {
    flex: 1,
    padding: 16,
    backgroundColor: "#25ABE1",
    alignItems: "center",
    justifyContent: "center",
  },
  layerTop: {
    flex: 2,
    // backgroundColor: opacity,
  },
  layerCenter: {
    flex: 1,
    flexDirection: "row",
  },
  layerLeft: {
    flex: 1,
    // backgroundColor: opacity,
  },
  focused: {
    flex: 1,
  },
  layerRight: {
    flex: 1,
    // backgroundColor: opacity,
  },
  layerBottom: {
    flex: 2,
    // backgroundColor: opacity,
  },
});
