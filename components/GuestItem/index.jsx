import { Flex } from "@react-native-material/core";
import { Text, View } from "react-native";
import React, { PureComponent } from "react";
import { Avatar, Button } from "react-native-paper";
import Icon from "react-native-vector-icons/AntDesign";
import * as SecureStore from "expo-secure-store";
import { fetchScan } from "../../network/lib/invitation";

export class GuestItem extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { isLoadingScan: false };
  }
  render() {
    const {
      _id,
      guest_name,
      guest_type,
      guest_label,
      presence,
      invitationId,
      navigation,
      bride_name,
      groom_name,
      main_image,
      onSuccess,
    } = this.props;

    const scanQR = async (guest_id) => {
      this.setState({
        isLoadingScan: true,
      });
      let token = await SecureStore.getItemAsync("token");
      await fetchScan(token, invitationId, {
        guest_id: guest_id,
        channel: 1,
      })
        .then((response) => {
          this.setState({
            isLoadingScan: false,
          });
          navigation.navigate("WelcomeGuest", {
            _id: invitationId,
            guest_name: response.data?.result?.guest_name,
            bride_name: bride_name,
            groom_name: groom_name,
            main_image: main_image,
          });
          onSuccess(true);
        })
        .catch((error) => {
          this.setState({
            isLoadingScan: false,
          });
          alert(`Gagal menambah tamu, ${error?.response?.data?.message}`);
        });
    };

    return (
      <View
        key={_id}
        style={{
          backgroundColor: "white",
          padding: 16,
          borderRadius: 8,
          marginBottom: 16,
        }}
      >
        <Flex
          direction="row"
          alignItems="center"
          justifyContent="space-between"
        >
          <Flex direction="row" alignItems="center">
            <Avatar.Icon
              size={32}
              icon="account"
              style={{
                marginRight: 8,
                backgroundColor: "#019FC4",
              }}
              color="white"
            />
            <Flex direction="column">
              <Text style={{ fontWeight: "700" }}>{guest_name}</Text>
              <Text style={{ fontSize: 12, color: "grey" }}>{guest_type}</Text>
              {guest_label ? (
                <>
                  <Text
                    style={{
                      fontSize: 12,
                      color: "grey",
                    }}
                  >
                    {guest_label}
                  </Text>
                </>
              ) : null}
            </Flex>
          </Flex>
          {!presence ? (
            <>
              <Button
                mode="outlined"
                textColor="#019FC4"
                style={{
                  borderColor: "#019FC4",
                  borderRadius: 8,
                }}
                compact
                //   onPress={showDialog}
                onPress={() => scanQR(_id)}
                loading={this.state.isLoadingScan}
              >
                Konfirmasi Kehadiran
              </Button>
            </>
          ) : (
            <>
              <Flex direction="column">
                <Flex direction="row" alignItems="center">
                  <Text
                    style={{
                      color: "green",
                      fontWeight: "700",
                      marginRight: 8,
                    }}
                  >
                    Check-In
                  </Text>
                  <Icon
                    name="checkcircle"
                    color="green"
                    style={{ fontSize: 18 }}
                  />
                </Flex>
              </Flex>
            </>
          )}
        </Flex>
      </View>
    );
  }
}
