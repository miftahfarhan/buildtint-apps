import axios from "axios";
import { API_URL } from "../utils/urls";
const axiosClient = axios.create({
    baseURL: `${API_URL}api/v1`,
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'apiKey': '1EFE983E8213125DAB88AA649E3FC',
    }
});

// axiosClient.interceptors.response.use(
//     (response) => {
//         return response;
//     }, (error) => {
//         let res = error.response;
//         if (res.status == 401) {
//             window.location.href = "https://example.com/login";
//         }
//         console.error("Looks like there was a problem. Status Code: " + res.status);
//         return Promise.reject(error);
//     }
// );

export default axiosClient

