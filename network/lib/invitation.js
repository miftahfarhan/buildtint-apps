import axiosClient from "../apiClient";

export function fetchInvitationList(token) {
    let config = {
        headers: {
            Authorization: `Bearer ${token}`,
        },
    };
    return axiosClient.get('/invitation/list', config);
}

export function fetchScan(token, id, data) {
    let config = {
        headers: {
            Authorization: `Bearer ${token}`,
        },
    };
    return axiosClient.post(`/invitation/${id}/scan`, data, config);
}

export function fetchInvitationGuest(token, id, page, search) {
    let config = {
        headers: {
            Authorization: `Bearer ${token}`,
        },
    };
    return axiosClient.get(`/invitation/${id}/guest?is_paginate=true&per_page=10&page=${page}&search=${search}`, config);
}

export function fetchAddScanGuest(token, id, data) {
    let config = {
        headers: {
            Authorization: `Bearer ${token}`,
        },
    };
    return axiosClient.post(`/invitation/${id}/add_and_scan_guest`, data, config);
}