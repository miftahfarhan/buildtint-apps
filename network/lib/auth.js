import axiosClient from "../apiClient";

export function fetchLogin(data) {
    return axiosClient.post('/login', data);
}

export function fetchLogout(token) {
    let config = {
        headers: {
            Authorization: `Bearer ${token}`,
        },
    };
    return axiosClient.get('/logout', config);
}